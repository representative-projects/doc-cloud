(function () {
    'use strict'

    function checkFilesOnPage() {
        let paginationButtons = $('.pagination_nav_menu__item'),
            items = $('.grid__item'),
            itemsPerPage = $('.pagination_nav_menu__list').data('item_count'),
            nextElement = $(paginationButtons).filter(":nth-child(2n)"),
            previousElement = $(paginationButtons).first(),
            nextPage = parseInt($(nextElement).attr('data-target_page'));
        switch (true) {
            case nextPage === 2 && itemsPerPage === items.length:
                $(previousElement).addClass('disabled');
                $(nextElement).removeClass('disabled');
                break;
            case nextPage > 2 && itemsPerPage !== items.length:
                $(nextElement).addClass('disabled');
                $(previousElement).removeClass('disabled');
                break;
            case nextPage > 2 && itemsPerPage === items.length:
                $(nextElement).removeClass('disabled');
                $(previousElement).removeClass('disabled');
                break;
            default:
                $(nextElement).addClass('disabled');
                break;
        }
    }

    function getPageFilesList(action) {
        let el = $('[data-pg_action="' + action + '"]'),
            targetPage = parseInt($(el).attr('data-target_page'));
        $.ajax({
            url: '/api/get/file-list',
            method: 'GET',
            data: {
                page: targetPage
            },
            success: function (response) {
                renderFileGrid(response.result.data);
                setTimeout(function () {
                    updatePaginationBlock(targetPage);
                }, 100);

            }
        });

    }

    function updatePaginationBlock(currentPage) {
        let paginationButtons = $('.pagination_nav_menu__item'),
            nextElement = $(paginationButtons).filter(":nth-child(2n)"),
            previousElement = $(paginationButtons).first();
        $(nextElement).attr("data-target_page", currentPage + 1);
        $(previousElement).attr("data-target_page", currentPage - 1);
        setTimeout(function () {
            checkFilesOnPage();
        }, 100);
    }

    function renderFileGrid(source) {
        let modals = $('.modal'),
            gridRow = $('.grid'),
            modalsRow = gridRow.next(),
            gridItems = $('.grid__item');
        gridItems.remove();
        modals.remove();
        $(source).each(function (index, element) {
            let item = $('.clone__item').clone(),
                modal = $('.clone__modal').clone();
            $(item)
                .removeClass('clone__item')
                .addClass('grid__item')
                .find('.file')
                .attr('data-target', '#preview-file_' + element.id)
                .find('img')
                .attr({src: element.preview_file_path, alt: element.name})
                .parent()
                .parent()
                .find('span')
                .html(element.name);
            $(modal)
                .removeClass('clone__modal')
                .addClass('modal')
                .attr('id', 'preview-file_' + element.id)
                .find('.preview__iframe_responsive')
                .attr('src', element.file_path);

            gridRow.find('.grid__body').append(item);
            modalsRow.append(modal);
        });
    }

    function init() {
        let paginationButtons = $('.pagination_nav_menu__item');
        checkFilesOnPage();
        paginationButtons.on('click', function (e) {
            let element = $(this);
            if (!$(element).hasClass('disabled')) {
                getPageFilesList(element.data('pg_action'));
            }
        })
    }

    $(document).ready(function () {
        $(this).scrollTop(0);
        init();
    });
}())