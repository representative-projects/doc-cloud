## Symfony document holder project
Single page app with thumbnails of the first page of PDF document. 4 x 5
thumbnails per page.
### Requirements
* [ImageMagick][imagick]
* [GhostScript][ghostscript]
* [MySql][mysql]
* [Composer][composer]
### OR
* Docker

### Install

##### Project
```
git clone git@gitlab.com:vlkinc/doc-cloud.git
cd doc-cloud
composer install
```
#### Local

##### GhostScript
```
sudo apt update
sudo apt install ghostscript
```
##### ImageMagick
```
sudo apt-get install php-imagick

# Restat apache/nginx
sudo service apache2 restart
# or
sudo service nginx restart

```
##### DB
Update db credentials in ```.env``` file and run:
```
bin/console doctrine:database:create
bin/console doctrine:schema:create
```
### Troubleshooting
```
# In case of error:
attempt to perform an operation not 
allowed by the security policy `PDF' @ 
error/constitute.c/IsCoderAuthorized/408
# Open the file 
sudo nano /etc/ImageMagick-6/policy.xml
# find and edit the line
<policy domain="coder" rights="none" pattern="PDF" />
# to :
<policy domain="coder" rights="read|write" pattern="PDF" />
```

### Docker

##### Linux scenario
```
# Instaling docker
curl -sSL https://get.docker.com/ | sh

# Instaling docker-compose
sudo curl -L --fail https://github.com/docker/compose/releases/download/1.27.4/run.sh -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Build project
docker-compose up -d

# Check docs container
docker container exec -it docs bash

# Create DB
bin/console doctrine:schema:create

# Check WEB http://localhost
```

[imagick]: https://www.php.net/imagick
[ghostscript]: https://www.ghostscript.com/download.html
[mysql]: https://www.mysql.com/downloads/
[composer]: https://getcomposer.org/download/
