<?php

namespace App\Controller;

use App\Entity\File;
use App\Form\FileUploaderType;
use App\Services\FileService;
use Exception;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @var FileService
     */
    private $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @Route("/", name="home")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function index(Request $request): Response
    {
        $file = new File();
        $form = $this->createForm(FileUploaderType::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->fileService->handleFile($form->get('allowed_mime_types')->getData());
            return $this->redirectToRoute('home');
        }

        return $this->render('main/index.html.twig', [
            'fileCollection' => $this->fileService->getFileCollection(),
            'uploadForm' => $form->createView(),
        ]);
    }


    /**
     * @Route(
     *     "/api/get/file-list",
     *     name="files_list",
     *     methods={"GET"})
     * @return JsonResponse
     */

    public function getPaginatedFileList(Request $request)
    {
        $serializer = SerializerBuilder::create()->build();
        $listCollection = json_decode(
            $serializer->serialize(
                $this->fileService->getFileCollection(
                    $request->get('page')
                ),
            'json'),
        true);
        return new JsonResponse([
            'result' => [
                'data' => $listCollection
                ]
            ]);

    }
}
