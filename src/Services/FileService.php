<?php


namespace App\Services;

use App\Entity\File;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Imagick;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{
    private $doctrine;
    private $uploadDir;
    private $absoluteUploadPath;
    private $previewDir;
    private $absolutePreviewPath;
    /**
     * @var UploadedFile
     */
    private $file;
    private $previewFilename;
    private $safeFilename;
    private $newFilename;
    private $offset = null;

    public function __construct(
        ManagerRegistry $managerRegistry,
        $uploadDir,
        $absoluteUploadPath,
        $previewDir,
        $absolutePreviewPath
    )
    {
        $this->doctrine = $managerRegistry;
        $this->uploadDir = $uploadDir;
        $this->absoluteUploadPath = $absoluteUploadPath;
        $this->previewDir = $previewDir;
        $this->absolutePreviewPath = $absolutePreviewPath;
    }

    public function getFileCollection($page = null)
    {
        $this->calculateOffset($page);
        return $this->doctrine->getRepository(File::class)->findBy(
            [],
            ['id' => 'ASC'],
            File::ITEMS_PER_PAGE,
            $this->offset
        );
    }

    /**
     * @param UploadedFile $file
     * @throws Exception
     */
    public function handleFile(UploadedFile $file)
    {
        if ($file) {
            $this->file = $file;

            $this->prepareFilename();
            $this->upload();
            $this->generatePreviewImage();
            $this->updateFileTable();
        }
    }

    private function prepareFilename() :void
    {
        $originalFilename = pathinfo($this->file->getClientOriginalName(), PATHINFO_FILENAME);
        $this->safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $uniqId = uniqid();
        $this->newFilename = $this->safeFilename .'-'. $uniqId .'.'. $this->file->guessExtension();
        $this->previewFilename = $this->safeFilename . '-' . $uniqId . '.jpg';
    }

    private function upload()
    {
        try {
            $this->file->move(
                $this->absoluteUploadPath,
                $this->newFilename
            );
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
            throw $e;
        }
    }

    private function generatePreviewImage()
    {
        $image = new Imagick($this->absoluteUploadPath . '/' . $this->newFilename . '[0]');
        $image->setImageFormat('jpg');
        $previewFile = fopen($this->absolutePreviewPath . '/' . $this->previewFilename, 'w+');
        $image->writeImageFile($previewFile, 'jpg');
        fclose($previewFile);
    }

    private function updateFileTable()
    {
        $entityManager = $this->doctrine->getManager();
        $fileEntity = new File();

        $fileEntity->setName($this->safeFilename);
        $fileEntity->setFilePath($this->uploadDir . '/' . $this->newFilename);
        $fileEntity->setPreviewPath($this->previewDir . '/' . $this->previewFilename);

        $entityManager->persist($fileEntity);
        $entityManager->flush();
    }

    private function calculateOffset($page) :void
    {
        if (!in_array($page, [null, 1], true)) {
            $this->offset = File::ITEMS_PER_PAGE * ((int) $page - 1);
        }
    }
}