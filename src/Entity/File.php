<?php

namespace App\Entity;

use App\Repository\FileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FileRepository::class)
 */
class File
{
    public const ITEMS_PER_PAGE = 20;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $file_path;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $preview_file_path;

    /**
     *  @Assert\NotBlank(message="Please choose file")
     *  @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $allowed_mime_types;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->file_path;
    }

    public function setFilePath(string $file_path): self
    {
        $this->file_path = $file_path;

        return $this;
    }

    public function getPreviewPath(): ?string
    {
        return $this->preview_file_path;
    }

    public function setPreviewPath(string $preview_file_path): self
    {
        $this->preview_file_path = $preview_file_path;

        return $this;
    }

    public function getAllowedMimeTypes(): ?string
    {
        return $this->allowed_mime_types;
    }

    public function setAllowedMimeTypes(string $allowed_mime_types): self
    {
        $this->allowed_mime_types = $allowed_mime_types;

        return $this;
    }
}
